import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'usuario' })
export class Usuario {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'usuario',
    nullable: true,
    type: 'varchar',
  })
  usuario: string;

  @Column({
    name: 'contrasena',
    nullable: true,
    type: 'varchar',
  })
  contrasena: string;

  @Column({
    name: 'idrol',
    nullable: true,
    type: 'int',
  })
  idRol: number;

  @Column({
    name: 'activo',
    nullable: true,
    type: 'int',
  })
  activo: number;

  @Column({
    name: 'lugar',
    nullable: true,
    type: 'int',
  })
  lugar: number;

  @Column({
    name: 'formm02',
    nullable: true,
    type: 'int',
  })
  formm02: number;

  @Column({
    name: 'formm03',
    nullable: true,
    type: 'int',
  })
  formm03: number;

  @Column({
    name: 'reliquidacion',
    nullable: true,
    type: 'int',
  })
  reliquidacion: number;

  @Column({
    name: 'comentariosdb',
    nullable: true,
    type: 'int',
  })
  comentariosBd: string;
}
