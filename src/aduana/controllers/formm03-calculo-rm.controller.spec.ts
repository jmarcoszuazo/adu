import { Test, TestingModule } from '@nestjs/testing';
import { Formm03CalculoRmController } from './formm03-calculo-rm.controller';

describe('Formm03CalculoRmController', () => {
  let controller: Formm03CalculoRmController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Formm03CalculoRmController],
    }).compile();

    controller = module.get<Formm03CalculoRmController>(Formm03CalculoRmController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
