import { Test, TestingModule } from '@nestjs/testing';
import { AduanaController } from './aduana.controller';

describe('AduanaController', () => {
  let controller: AduanaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AduanaController],
    }).compile();

    controller = module.get<AduanaController>(AduanaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
