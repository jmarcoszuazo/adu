import { Controller, Get } from '@nestjs/common';
import { Formm03CalculoRmService } from '../services/formm03-calculo-rm.service';

@Controller('formm03-calculo-rm')
export class Formm03CalculoRmController {
  constructor(
    private readonly formm03CalculoRmService: Formm03CalculoRmService,
  ) {}

  @Get()
  findAll() {
    return this.formm03CalculoRmService.findAll();
  }
}
