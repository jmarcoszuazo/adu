import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Operador } from '../entities/operador.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OperadorService {
  constructor(
    @InjectRepository(Operador)
    private readonly operadorRepository: Repository<Operador>,
  ) {}

  findAll() {
    return this.operadorRepository.find();
  }
}
