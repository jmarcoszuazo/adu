import { Test, TestingModule } from '@nestjs/testing';
import { Formm03CalculoRmService } from './formm03-calculo-rm.service';

describe('Formm03CalculoRmService', () => {
  let service: Formm03CalculoRmService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Formm03CalculoRmService],
    }).compile();

    service = module.get<Formm03CalculoRmService>(Formm03CalculoRmService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
