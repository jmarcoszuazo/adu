import { Module } from '@nestjs/common';
import { AduanaService } from './services/aduana.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Formm03 } from './entities/formm_03.entity';
import { AduanaController } from './controllers/aduana.controller';
import { OperadorService } from './services/operador.service';
import { OperadorController } from './controllers/operador.controller';
import { Operador } from './entities/operador.entity';
import { Formm03CalculoRmService } from './services/formm03-calculo-rm.service';
import { Formm03CalculoRm } from './entities/formm03-calculo-rm.entity';
import { Formm03CalculoRmController } from './controllers/formm03-calculo-rm.controller';
import { AduanaSalida } from './entities/aduana-salida.entity';

@Module({
  controllers: [
    AduanaController,
    OperadorController,
    Formm03CalculoRmController,
  ],
  providers: [AduanaService, OperadorService, Formm03CalculoRmService],
  imports: [
    TypeOrmModule.forFeature([
      Formm03,
      Operador,
      Formm03CalculoRm,
      AduanaSalida,
    ]),
  ],
})
export class AduanaModule {}
