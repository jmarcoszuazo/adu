import { type } from 'os';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Formm03 } from './formm_03.entity';

@Entity({ name: 'formm03calculorm' })
export class Formm03CalculoRm {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'idformm03',
    type: 'int',
  })
  idFormm03: number;

  @Column({
    name: 'elemento',
    type: 'varchar',
  })
  elemento: string;

  @Column({
    name: 'codigonandina',
    type: 'varchar',
  })
  codigoNandina: string;

  @Column({
    name: 'leyvalor',
    type: 'numeric',
  })
  leyValor: number;

  @Column({
    name: 'leyunidad',
    type: 'varchar',
  })
  leyUnidad: string;

  @Column({
    name: 'pesofinokg',
    type: 'numeric',
  })
  pesoFinoKg: number;

  @Column({
    name: 'pesofino',
    type: 'numeric',
  })
  pesoFino: number;

  @Column({
    name: 'cotizacionmineral',
    type: 'numeric',
  })
  cotizacionMineral: number;

  @Column({
    name: 'cotizacionunidad',
    type: 'varchar',
  })
  cotizacionUnidad: string;

  @Column({
    name: 'valorbrutousd',
    type: 'numeric',
  })
  valorBrutoUsd: number;

  @Column({
    name: 'valorbrutobs',
    type: 'numeric',
  })
  valorBrutoBs: number;

  @Column({
    name: 'alicuotaexterna',
    type: 'numeric',
  })
  alicuotaExterna: number;

  @Column({
    name: 'importermusd',
    type: 'numeric',
  })
  importeRmUsd: number;

  @Column({
    name: 'importermbs',
    type: 'numeric',
  })
  importeRmBs: number;

  @Column({
    name: 'idmineral',
    type: 'int',
  })
  idMineral: number;

  @Column({
    name: 'idexportador',
    type: 'int',
  })
  idExportador: number;

  @Column({
    name: 'idnandina',
    type: 'int',
  })
  idNandina: number;

  @ManyToOne(() => Formm03, (formm03) => formm03.formm03CalculoRm)
  @JoinColumn({ name: 'idformm03', referencedColumnName: 'id' })
  formm03: Formm03;
}
